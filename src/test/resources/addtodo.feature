Feature: Add Todo
  Scenario: Create todo by POST /todos
    Given ensure rest endpoint of "54.254.174.173:8080" is up
    When a POST request to /todos is made
    And the request body is
      """
{
  "targetDate": "2020-05-03",
  "description": "Devops expert",
  "user": "Lynda",
  "done": true
}
      """
    Then a 201 response is returned within 2000ms